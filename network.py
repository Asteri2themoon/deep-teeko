import keras
from keras import losses
from keras import optimizers
from keras.initializers import glorot_uniform,glorot_normal,he_normal,he_uniform
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Activation, Dropout, MaxPooling2D,LeakyReLU
import numpy as np
from game import *
import math
import time

gamma=0.9
learningRate=0.0001
randomPick=0.1
maxReward,minReward=1,-1

def reward(S):
	s1,s2=toNumber(S)
	if s1 in win:
		return 1
	if s2 in win:
		return -1
	return 0

#model

model=Sequential()
model.add(Conv2D(32, kernel_size=(2,2),
				 data_format='channels_first',
				 kernel_initializer=he_normal(),
				 bias_initializer=he_normal(),
				 activation='relu',
				 input_shape=(2,5,5)))
#model.add(LeakyReLU(0.3))
model.add(Conv2D(16, kernel_size=(2,2),
				 kernel_initializer=he_normal(),
				 bias_initializer=he_normal(),
				 activation='relu',
				 data_format='channels_first'))
#model.add(LeakyReLU(0.3))
model.add(Conv2D(16, kernel_size=(2,2),
				 kernel_initializer=he_normal(),
				 bias_initializer=he_normal(),
				 activation='relu',
				 data_format='channels_first'))
model.add(Flatten())
model.add(Dropout(0.3))
model.add(Dense(256,
				 kernel_initializer=he_normal(),
				 bias_initializer=he_normal(),
				 activation='relu'))
#model.add(LeakyReLU(0.3))
model.add(Dense(1,
				 kernel_initializer=he_normal(),
				 bias_initializer=he_normal()))
model.compile(loss=losses.mean_squared_error,
			  optimizer=optimizers.Adam(lr=learningRate))
model.summary()

def _enumerate(lst,first=None,step=1,last=None):
	enum=[]
	if last!=None:
		last+=step
		if step>=1 and last==0:
				last=None
		if step<=-1 and last==-1:
				last=None
	if first==None:
		first=0 if step>0 else -1
	for i,v in enumerate(lst[first:last:step]):
		enum.append((first+i*step,v))
	return enum

def calculateValue(games,reversed=False,_model=model):
	values=[]
	lastState=0 if reversed else -1
	nextState=1 if reversed else -1
	for i,endGame in enumerate(games):
		epi=np.zeros(len(endGame))
		epi[lastState]=reward(endGame[lastState])
		winSide=epi[lastState]>0
		enum=_enumerate(endGame,last=-2) if reversed else _enumerate(endGame,first=-1,step=-1,last=1)
		for j,S in enum:
			stats=np.array(nextPossibleStats(S))
			V=_model.predict(stats)
			if (j&1)==0 and winSide:
				epi[j+nextState]=reward(endGame[j+nextState])+gamma*np.max(V)
				if epi[j+nextState]>maxReward:
					epi[j+nextState]=maxReward
			else:
				epi[j+nextState]=reward(endGame[j+nextState])+gamma*np.min(V)
				if epi[j+nextState]<minReward:
					epi[j+nextState]=minReward
		values.append(epi)
	return values

def minmax(Si,n,max=True,level=0,alpha=-math.inf,beta=math.inf):
	end=reward(Si)
	if end==minReward or end==maxReward:
		return end
	if n<=0:
		value=model.predict(Si.reshape((1,2,5,5)))
		return float(value)
	else:
		nextS=nextPossibleStats(Si,reverse=max)
		if not max:
			value=math.inf
			for stat in nextS:
				value=np.min([value,minmax(stat,n-1,not max,level+1,alpha,beta)])
				if alpha>=value:
					return value
				beta=np.min([beta,value])
		else:
			value=-math.inf
			for stat in nextS:
				value=np.max([value,minmax(stat,n-1,not max,level+1,alpha,beta)])
				if value>=beta:
					return value
				alpha=np.max([alpha,value])
		return value

def play(S0,n,reversed=False):
	nextS=nextPossibleStats(S0,reverse=not reversed)
	values=[]
	for stat in nextS:
		value=minmax(stat,n-1,reversed)
		if value is None:
			if reversed:
				values.append(2)
			else:
				values.append(-2)
		else:
			values.append(value)
		
	if reversed:
		return nextS[np.argmin(values)]
	else:
		return nextS[np.argmax(values)]