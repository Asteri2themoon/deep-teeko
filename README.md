# Deep Teeko
_Une intelligence artificielle qui utilise le deep learning et le reinforcement learning pour apprendre à jouer au Teeko_

## Règles du jeu

Le jeu se déroule sur un plateau de cinq cases par cinq. Chaque partie se déroule entre deux adversaires, l'un qui joue avec des pions noir et l'autre avec des pions rouge. Les deux joueurs vont dans un premier temps placer un pion, chacun leurs tours. Une fois que les joueurs auront placé quatre pions, ils pourront déplacer un pion à tour de rôle sur une case adjacente, les déplacements en diagonales sont aussi autorisé.

![alt text](img/teeko-move.png)

Le but du jeu est d'aligner les quarts pions, horizontalement, verticalement ou diagonalement. Il est aussi possible de gagner en faisant un carré avec quatre pions.

![alt text](img/teeko-win.png)

## Reinforcement learning

Le but du projet est de créer une intelligence artificielle qui va apprendre à jouer au Teeko sans aucune connaissance experte. Au début de l'entraînement, l'IA n'a donc aucune connaissance des règles du jeu, elle dispose uniquement de la liste des coups possible à jouer.

La technique utilisée pour l'apprentissage par renforcement est le Value learning.

La récompense est définie tel que :

$`\begin{array}{r l}
reward: & S\to\mathbb{R}\\
 & s \mapsto   \left \{
   \begin{array}{l}
      1 \text{ si la partie est gagné} \\
      -1 \text{ si la partie est perdu} \\
      0 \text{ sinon}
   \end{array}
   \right . 
   \end{array}
`$

L'équation de Bellman est utilisée pour définie la Value d'un état donnée du plateau de jeu.

$` V(x_0)=\sum_{i=0}^\infty \gamma^ir(x_i)`$

Le but de la phase d'entraînement étant de trouver la Value dans le cas où les joueurs jouent de façon optimale, on définie une relation de récurrence pour faire converger la Value actuelle vers la Value optimale $` V^*`$. 

$` V^*(x_i)=r(x_i)+\gamma\max_{a\in A}V^*(f(x_i,a)) `$

On fixe $`\gamma`$ la discount-rate à 0,9.

La politique utilisée est simplement exprimée à partir de $`V^*`$ et définie tel que :

$`\pi^*(x)=arg\max_{a\in A}V^*(f(x,a))`$

Deux phases d'apprentissage sont mises en place.
* Pendant la première phase, comme la Value est totalement aléatoire au début de l'entraînement la génération d'épisode est assez difficile, l'algorithme génère donc des fins de parties aléatoires à partir d'une position gagnant ou perdante. Une estimation de la Value de toutes les positions est calculé avec l'équation de Bellman.
* Pendant la seconde phase, l'algorithme génère des épisodes. La Value est mise à jour en calculant une estimation à partir de l'état suivant. Pendant la génération des épisodes, on fait jouer 10% des coups aléatoirement pour satisfaire le besoin d'exploration.

## Deep learning

La Value est apprise par un réseau de neurones. Les premières couches sont des couches de convolution, le but étant de détecter les paternes que formes les pions sur le plateau. Une couche de dropout est utilisé pour éviter d'avoir trop de problème de surapprentissage. Les dernières couches sont des couches totalement connectées classique.

![alt text](img/teeko-value.png)

L'entraînement du réseau de neurones est fait avec la méthode des moindres carrés et l'algorithme d'optimisation utilisé est Adam avec une vitesse d'apprentissage de 0,0001.

## Utilisation

Lancer une partie contre l'intelligence artificielle : (placez et déplacez vos pions en cliquant directement dessus)
```sh
python play.py
```

Effectuer la première phase d'entraînement sur les fins de partie :
```sh
python trainEndGame.py
```

Effectuer la seconde phase d'entraînement sur les épisodes généré en jouant contre elle-même :
```sh
python train.py
```

L'entraînement peut être interrompu pour terminer l'apprentissage, des sauvegardes régulières du réseau de neurones sont effectuées.

Le projet fonctionne avec python 3 et les bibliothèques :
* Tensorflow
* Keras
* numpy
* pandas
* curses
