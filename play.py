import keras
from keras import losses
from keras import optimizers
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Activation, Dropout, MaxPooling2D,LeakyReLU
import numpy as np
import matplotlib.pyplot as plt
import time
import curses
from game import *
from network import *

#model.load_weights('model/endGameTrainTEST_minMaxVertion_13390.h5')
model.load_weights('model/step_minMaxVertion_262427.h5')

def callback(screen):
	screen.clear()
	curses.mousemask(curses.ALL_MOUSE_EVENTS)
	curses.noecho()
	curses.cbreak()
	curses.curs_set(0)
	curses.start_color()
	curses.use_default_colors()
	curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_WHITE, 240)

	S=[np.zeros((5,5)),np.zeros((5,5))]
	side=np.random.choice([True,False],1)
	it=0
	while reward(S)==0:
		plotGame(screen,S)
		if ((it&1)==0 and side) or ((it&1)!=0 and (not side)):
			valid,x0,y0,x1,y1=False,-1,-1,-1,-1
			while not valid:
				getch=screen.getch()
				while getch!=curses.KEY_MOUSE:
					getch=screen.getch()
				_,x,y,_,bstate=curses.getmouse()
				x=int(x/2)
				if bstate==curses.BUTTON1_CLICKED:
					if x0==-1:
						if x<5 and y<5:
							if S[0 if side else 1][y,x]==1 and S[0 if side else 1].sum()==4:
								x0,y0=x,y
								plotGame(screen,S,select=(x0,y0))
							if S[0][y,x]==0 and S[1][y,x]==0 and S[0 if side else 1].sum()<4:
								x0,y0=x,y
								x1,y1=x,y
								valid=True
					else:
						if x<5 and y<5:
							if x==x0 and y==y0:
								x0,y0=-1,-1
								plotGame(screen,S)
							elif S[0][y,x]==0 and S[1][y,x]==0 and abs(x-x0)<=1 and abs(y-y0)<=1:
								x1,y1=x,y
								valid=True
			S[0 if side else 1][y0,x0]=0
			S[0 if side else 1][y1,x1]=1
			plotGame(screen,S)
			screen.refresh()
			#time.sleep(0.5)
		else:
			S=play(S,4,reversed=side)
		it+=1
	plotGame(screen,S)
	screen.refresh()
	screen.addstr(5,0,'   WIN' if ((reward(S)==1 and side) or (reward(S)==-1 and (not side))) else '  LOST')
	screen.getch()


curses.wrapper(callback)