import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from game import *
from network import *
import shutil
import os

def generateEpisode(max=-1):
	S=[np.zeros((5,5)),np.zeros((5,5))]
	episode=[]
	it=0
	limite=max
	side=np.random.choice([True,False],1)
	while reward(S)==0:
		Si=np.copy(S)
		if ((it&1)==0 and side) or ((it&1)!=0 and (not side)):
			if np.random.random()<randomPick:
				stats=np.array(nextPossibleStats(S,reverse=side))
				S=stats[np.random.choice(len(stats))]
			else:
				S=play(S,1,reversed=not side)
		else:
			S=play(S,1,reversed=side)
		episode.append(Si)
		it+=1
		print("generate game {}    ".format(it),end='\r')
		if max!=-1 and it>limite:
			return generateEpisode(max=limite)
	episode.append(S)
	return np.array(episode)

logsDir='./logs'

if os.path.exists(logsDir):
	#shutil.rmtree(logsDir)
        pass
tbCallBack = keras.callbacks.TensorBoard(log_dir=logsDir, write_graph=False, write_images=False)

model.load_weights('model/step_minMaxVertion_38190.h5')

for i in range(1000000):
	print('generate game     ',end='\r')
	X=generateEpisode(max=1000)
	X=X[-50:]
	
	print('calculate values  ',end='\r')
	values=calculateValue([X])
	Y=np.array(values).reshape((-1))
	
	print('save game         ',end='\r')
	pd.DataFrame(data=[list(s.reshape(50))+[v] for s,v in zip(X,Y)]).to_csv('game/step{}.csv'.format(i))
	
	print('train network     ',end='\r')
	history=model.fit(X,Y, epochs=20, batch_size=32,verbose=0, callbacks=[tbCallBack])
	print('step {0} (moves:{1},loss:{2:.5f})'.format(i,len(Y),history.history['loss'][-1]))
	
	print('save network      ',end='\r')
	model.save_weights('model/step_minMaxVertion_{}.h5'.format(i))
