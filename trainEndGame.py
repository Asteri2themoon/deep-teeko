import keras
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os
from game import *
from network import *
import shutil

def generateRandomEnd(n):
	endGame=[]
	for _ in range(n):
		valid=True
		end=toMatrix(np.random.choice(list(win.keys()),1))
		ennemy=np.zeros((5,5),dtype=bool)
		pieces=0
		while pieces<4:
			[x,y]=np.random.randint(5,size=2)
			if ennemy[x,y]==False and end[x,y]==False:
				ennemy[x,y]=True
				if reward([ennemy,ennemy])!=0:
					ennemy[x,y]=False
				else:
					pieces+=1
		side=np.random.choice([True,False],1)
		S=np.array([end,ennemy]).reshape((2,5,5)) if side else np.array([ennemy,end]).reshape((2,5,5))
		game=[S]
		for j in range(9):
			valid=False
			while not valid:
				stats=nextPossibleStats(S,reverse=side)
				rand=np.random.randint(len(stats),size=1)[0]
				S1=stats[rand]
				if reward(S1)==0:
					valid=True
					S=S1
			side=not side
			game.append(S)
		endGame.append(game)
	return endGame

shutil.rmtree('./logs')
tbCallBack = keras.callbacks.TensorBoard(log_dir='./logs', write_graph=False, write_images=False)

for k in range(1000000):
	print('generate end game',end='\r')
	endGames=generateRandomEnd(200)
	values=[]
	print('calculate value     ',end='\r')
	values=calculateValue(endGames,reversed=True,_model=model)
	Y=np.array(values).reshape((-1))
	X=np.array(endGames).reshape((-1,2,5,5))
	print('train network      ',end='\r')
	history=model.fit(X,Y, epochs=10, batch_size=32,verbose=0, callbacks=[tbCallBack])
	print('step:{0} loss:{1:.5f}'.format(k,history.history['loss'][-1]))
	model.save_weights('model/endGameTrainTEST_minMaxVertion_{}.h5'.format(k))
