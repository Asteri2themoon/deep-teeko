import numpy as np
import curses

win={491520: True, 6336: True, 532610: True, 135300: True, 31457280: True, 8521760: True, 17043520: True, 4329600: True, 396: True, 1118464: True, 15: True, 541200: True, 12672: True, 17318400: True, 792: True, 30: True, 12976128: True, 1082400: True, 33825: True, 6488064: True, 198: True, 3168: True, 405504: True, 25344: True, 270600: True, 25952256: True, 811008: True, 34952: True, 960: True, 266305: True, 67650: True, 202752: True, 15360: True, 2164800: True, 8659200: True, 983040: True, 480: True, 69904: True, 99: True, 101376: True, 2236928: True, 15728640: True, 30720: True, 3244032: True}

def getMoves(S,pos):
    moves=[]
    for nPos in [pos+m for m in [[1,0],[1,1],[0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1]]]:
        if nPos[0]>=0 and nPos[0]<5 and nPos[1]>=0 and nPos[1]<5:
            if S[0][nPos[0],nPos[1]]==0 and S[1][nPos[0],nPos[1]]==0:
                moves.append(nPos)
    return moves

M=np.power(2*np.ones(25),np.array(list(range(25))))
def toNumber(S):
    return np.dot(M,S[0].reshape(25)),np.dot(M,S[1].reshape(25))

M2=np.power(2*np.ones((5,5)),np.array(list(range(25))).reshape((5,5)))
def toMatrix(n):
    res=np.zeros((5,5),dtype=bool)
    for x in range(5):
        for y in range(5):
            res[x,y]=(n&int(M2[x,y]))!=0
    return res

def nextPossibleStats(S,reverse=False):
    pieces=[]
    nextStats=[]
    side=0 if reverse else 1
    for y in range(5):
        for x in range(5):
            if S[side][y,x]==1:
                pieces.append(np.array([y,x]))
    if len(pieces)<4:
        for y in range(5):
            for x in range(5):
                if S[side][y,x]==0 and S[1-side][y,x]==0:
                    nStat=np.copy(S)
                    nStat[side][y,x]=1
                    nextStats.append(nStat)
    else:
        for p in pieces:
            for m in getMoves(S,p):
                nStat=np.copy(S)
                nStat[side][p[0],p[1]]=0
                nStat[side][m[0],m[1]]=1
                nextStats.append(nStat)
    return nextStats

def hasError(S):
    return np.sum(np.logical_and(S[0],S[1]))!=0

def plotGame(screen,S,select=None):
    for y in range(5):
        for x in range(5):
            color_pair=1
            if select!=None:
                (sx,sy)=select
                if sx==x and sy==y:
                    color_pair=2
            if S[0][y,x]==1:
                screen.addstr(y,x*2,'*', curses.color_pair(color_pair))
            elif S[1][y,x]==1:
                screen.addstr(y,x*2,'#', curses.color_pair(color_pair))
            else:
                screen.addstr(y,x*2,'0', curses.color_pair(color_pair))